# 1.0.0 (2019-11-06)


### Features

* Added test  coverage to satisfactory level ([bbb0cb0](https://gitlab.com/dreamer-labs/libraries/jinja2-ansible-filters/commit/bbb0cb0))
* Initial commit with full function and tests ([529c606](https://gitlab.com/dreamer-labs/libraries/jinja2-ansible-filters/commit/529c606))
